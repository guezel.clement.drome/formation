@extends('layout')


@section('title_page') Show User @endsection



@section('css')


@endsection


@section('content')

<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-chat-1 text-primary"></i>
            </span>
            <h3 class="card-label">
                Ajouter un utilisateur
            </h3>
        </div>
    </div>
    <form class="form" action="{{route('user.update')}}" method="POST">
        @csrf
        <div class="card-body">
            <input name="user[user_id]" type="hidden" value="{{$user->id}}" />
            <div class="form-group">
                <label>Nom :</label>
                <input name="user[last_name]" type="text" class="form-control form-control-solid" placeholder="Enter full name" value="{{$user->last_name}}"/>
            </div>
            <div class="form-group">
                <label>Prénom :</label>
                <input name="user[first_name]" type="text" class="form-control form-control-solid" placeholder="Enter full name" value="{{$user->first_name}}"/>
            </div>
            <div class="form-group">
                <label>Usine :</label>
                <select name="user[factory_id]" class="form-control form-control-solid">
                    @foreach($factories as $factory)
                    <option @if($factory->id_factory == $user->factory_id) selected @endif value="{{$factory->id_factory}}">{{$factory->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Adresse mail :</label>
                <input name="user[email]" type="email" class="form-control form-control-solid" placeholder="Enter email" value="{{$user->email}}"/>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">Modifier</button>
            <button type="reset" class="btn btn-secondary">Annuler</button>
        </div>
    </form>
</div>

@endsection


@section('script')

<script type="text/javascript">



</script>

@endsection
