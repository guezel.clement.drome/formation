@extends('layout')


@section('title_page') User List @endsection



@section('css')


@endsection


@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">
                Liste des utilisateurs
            </h3>
        </div>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Email</th>
                    <th>Usine</th>
                    <th>Droit</th>
                    <th>Date de création</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->last_name}}</td>
                    <td>{{$user->first_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->factory ? $user->factory->name : ""}}</td>
                    <td>{!! $user->is_admin ? '<span class="label label-sm label-sucess label-pill label-inline mr-2">Admin</span>' : '<span class="label label-sm label-danger label-pill label-inline mr-2">Standard</span>' !!}</td>
                    <td>{{date("d/m/Y",strtotime($user->created_at))}}</td>
                    <td class="d-flex">
                        <form class="form" action="{{route('user.delete')}}" method="POST">
                            @method('delete')
                            @csrf
                            <input name="user_id" type="hidden" class="btn btn-primary mr-2" value="{{$user->id}}"/>
                            <button type="submit" class="btn btn-danger btn-icon mr-2"><i class="flaticon2-trash"></i></button>
                        </form>
                            <a href="{{route('user.show',['user' => $user->id])}}" class="btn btn-info btn-icon mr-2"><i class="flaticon2-pen"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection


@section('script')

<script type="text/javascript">



</script>

@endsection
