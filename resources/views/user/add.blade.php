@extends('layout')


@section('title_page') Add User @endsection



@section('css')


@endsection


@section('content')

<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-chat-1 text-primary"></i>
            </span>
            <h3 class="card-label">
                Ajouter un utilisateur
            </h3>
        </div>
    </div>
    <form class="form" action="{{route('user.store')}}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label>Nom :</label>
                <input name="user[last_name]" type="text" class="form-control form-control-solid" placeholder="Enter full name"/>
            </div>
            <div class="form-group">
                <label>Prénom :</label>
                <input name="user[first_name]" type="text" class="form-control form-control-solid" placeholder="Enter full name"/>
            </div>
            <div class="form-group">
                <label>Usine :</label>
                <select name="user[factory_id]" class="form-control form-control-solid">
                    @foreach($factories as $factory)
                    <option value="{{$factory->id_factory}}">{{$factory->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Adresse mail :</label>
                <input name="user[email]" type="email" class="form-control form-control-solid" placeholder="Enter email"/>
                <span class="form-text text-muted">We'll never share your email with anyone else</span>
            </div>
            <div class="form-group">
                <label>Mot de passe :</label>
                <input name="user[password]" type="password" class="form-control form-control-solid"/>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">Créer</button>
            <button type="reset" class="btn btn-secondary">Annuler</button>
        </div>
    </form>
</div>

@endsection


@section('script')

<script type="text/javascript">



</script>

@endsection
