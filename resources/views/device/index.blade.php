@extends('layout')


@section('title_page') Index @endsection



@section('css')


@endsection


@section('content')

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Liste des Appareil
                </h3>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom Français</th>
                    <th>Nom Anglais</th>
                    <th>Nom Allemand</th>
                    <th>Version</th>
                    <th>Type</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($devices as $device)
                    <tr>
                        <td>{{$device->id_device}}</td>
                        <td>{{$device->name_fr}}</td>
                        <td>{{$device->name_en}}</td>
                        <td>{{$device->name_de}}</td>
                        <td>{{$device->price}}</td>
                        <td>{{date("d/m/Y",strtotime($device->created_at))}}</td>
                        <td class="d-flex">
                            <form class="form" action="{{route('device.destroy',['id' => $device->id_device])}}" method="POST">
                                @method('delete')
                                @csrf
                                <input name="id_device" type="hidden" class="btn btn-primary mr-2" value="{{$device->id_device}}"/>
                                <button type="submit" class="btn btn-danger btn-icon mr-2"><i class="flaticon2-trash"></i></button>
                            </form>
                            <a href="{{route('device.show',['device' => $device->id_device])}}" class="btn btn-info btn-icon mr-2"><i class="flaticon2-pen"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('script')

    <script type="text/javascript">



    </script>

@endsection
