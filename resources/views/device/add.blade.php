@extends('layout')


@section('title_page') Index @endsection



@section('css')


@endsection


@section('content')
    <div class="card">
        <div class="card-header">Ajouter un appareil</div>
        <form class="form" action="{{route('device.store')}}" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Nom Français :</label>
                    <input name="device[name_fr]" type="text" class="form-control"
                           placeholder="Entrer un nom d'appareil"/>
                </div>
                <div class="form-group">
                    <label>Nom Anglais :</label>
                    <input name="device[name_en]" type="text" class="form-control"
                           placeholder="Entrer un nom d'appareil"/>
                </div>
                <div class="form-group">
                    <label>Nom Allemand :</label>
                    <input name="device[name_de]" type="text" class="form-control"
                           placeholder="Entrer un nom d'appareil"/>
                </div>
                <div class="form-group">
                    <label>Version :</label>
                    <input name="device[version]" type="text" class="form-control"
                           placeholder="Entrer une version d'appareil"/>
                </div>
                <div class="form-group">
                    <label>Type :</label>
                    <select class="form-control" name="device[device_type_id]">
                        @foreach($deviceTypes as $deviceType)
                            <option value="{{$deviceType->id_device_type}}">{{$deviceType->name_fr}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Composant :</label>
                    <select multiple class="form-control select2" id="kt_select2_1" name="id_component[]">
                        @foreach($components as $component)
                            <option value="{{$component->id_component}}">{{$component->name_fr}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary mr-2">Créer</button>
                <button type="reset" class="btn btn-secondary">Annuler</button>
            </div>
        </form>
    </div>


@endsection


@section('script')

    <script type="text/javascript">
        $(".select2").select2();


    </script>

@endsection
