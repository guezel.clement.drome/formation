@extends('layout')


@section('title_page') Show User @endsection



@section('css')


@endsection


@section('content')

<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-chat-1 text-primary"></i>
            </span>
            <h3 class="card-label">
                Modifier un composant
            </h3>
        </div>
    </div>
    <form class="form" action="{{route('component.update')}}" method="POST">
        @csrf
        <div class="card-body">
            <input name="component[id_component]" type="hidden" value="{{$component->id_component}}" />
            <div class="form-group">
                <label>Nom du composant :</label>
                <input name="component[name_fr]" type="text" class="form-control form-control-solid" value="{{$component->name_fr}}" />
            </div>
            <div class="form-group">
                <label>Nom du composant :</label>
                <input name="component[name_en]" type="text" class="form-control form-control-solid" value="{{$component->name_en}}" />
            </div>
            <div class="form-group">
                <label>Nom du composant :</label>
                <input name="component[name_de]" type="text" class="form-control form-control-solid" value="{{$component->name_de}}" />
            </div>
            <div class="form-group">
                <label>Prix :</label>
                <input name="component[price]" type="text" class="form-control form-control-solid" value="{{$component->price}}" />
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">Modifier</button>
            <button type="reset" class="btn btn-secondary">Annuler</button>
        </div>
    </form>
</div>

@endsection


@section('script')

<script type="text/javascript">



</script>

@endsection
