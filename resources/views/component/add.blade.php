@extends('layout')


@section('title_page') Add Component @endsection



@section('css')


@endsection


@section('content')

    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-chat-1 text-primary"></i>
            </span>
                <h3 class="card-label">
                    Ajouter un composant
                </h3>
            </div>
        </div>
        <form class="form" action="{{route('component.store')}}" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Nom Français :</label>
                    <input name="component[name_fr]" type="text" class="form-control form-control-solid" placeholder="Entrer un nom de composant"/>
                </div>
                <div class="form-group">
                    <label>Nom Anglais :</label>
                    <input name="component[name_en]" type="text" class="form-control form-control-solid" placeholder="Entrer un nom de composant"/>
                </div>
                <div class="form-group">
                    <label>Nom Allemand :</label>
                    <input name="component[name_de]" type="text" class="form-control form-control-solid" placeholder="Entrer un nom de composant"/>
                </div>
                <div class="form-group">
                    <label>Prix :</label>
                    <input name="component[price]" type="number" step="0.01" class="form-control form-control-solid" placeholder="Entrer un prix"/>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary mr-2">Créer</button>
                <button type="reset" class="btn btn-secondary">Annuler</button>
            </div>
        </form>
    </div>

@endsection


@section('script')

    <script type="text/javascript">



    </script>

@endsection
