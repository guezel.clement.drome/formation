@extends('layout')


@section('title_page') Component List @endsection



@section('css')


@endsection


@section('content')

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Liste des composants
                </h3>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom Français</th>
                    <th>Nom Anglais</th>
                    <th>Nom Allemand</th>
                    <th>Prix</th>
                    <th>Date de création</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($components as $component)
                    <tr>
                        <td>{{$component->id_component}}</td>
                        <td>{{$component->name_fr}}</td>
                        <td>{{$component->name_en}}</td>
                        <td>{{$component->name_de}}</td>
                        <td class="text-right">{{\App\Helpers\UtilHelper::displayPrettyPrice($component->price)}}</td>
                        <td>{{date("d/m/Y",strtotime($component->created_at))}}</td>
                        <td class="d-flex">
                            <form class="form" action="{{route('component.delete')}}" method="POST">
                                @method('delete')
                                @csrf
                                <input name="id_component" type="hidden" class="btn btn-primary mr-2" value="{{$component->id_component}}"/>
                                <button type="submit" class="btn btn-danger btn-icon mr-2"><i class="flaticon2-trash"></i></button>
                            </form>
                            <a href="{{route('component.show',['component' => $component->id_component])}}" class="btn btn-info btn-icon mr-2"><i class="flaticon2-pen"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection


@section('script')

    <script type="text/javascript">



    </script>

@endsection
