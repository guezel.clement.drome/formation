@extends('layout')


@section('title_page') Add Order @endsection



@section('css')


@endsection


@section('content')

Créer une commande
<form class="form">
    <div class="card-body">
        <div class="form-group">
            <label>Numéro de commande :</label>
            <input type="order" class="form-control form-control-solid" placeholder="Entrer un numéro de commande"/>
        </div>
        <div class="form-group">
            <label>Usine :</label>
            <select class="form-control">
                <option>KSAS</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
        <div class="form-group">
            <label>Client :</label>
            <input type="client" class="form-control form-control-solid" placeholder="Entrer un nom de client"/>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary mr-2">Créer</button>
        <button type="reset" class="btn btn-secondary">Annuler</button>
    </div>
</form>


<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-chat-1 text-primary"></i>
            </span>
            <h3 class="card-label">
                Ajouter un utilisateur
            </h3>
        </div>
    </div>
    <form class="form">
        <div class="card-body">
            <div class="form-group">
                <label>Nom :</label>
                <input type="name" class="form-control form-control-solid" placeholder="Enter full name"/>
            </div>
            <div class="form-group">
                <label>Adresse mail :</label>
                <input type="email" class="form-control form-control-solid" placeholder="Enter email"/>
                <span class="form-text text-muted">We'll never share your email with anyone else</span>
            </div>
            <div class="form-group">
                <label>Mot de passe :</label>
                <input type="password" class="form-control form-control-solid"/>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">Créer</button>
            <button type="reset" class="btn btn-secondary">Annuler</button>
        </div>
    </form>
</div>

@endsection


@section('script')

<script type="text/javascript">



</script>

@endsection
