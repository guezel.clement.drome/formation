<?php

namespace App\Http\Controllers;

use App\Http\Requests\ComponentRequest;
use App\Models\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ComponentController extends Controller
{

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){

        $components = Component::orderBy('name_fr')->get();

        return view('component.index', [
            'components' => $components
        ]);

    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(){

        // Recupérer les composants

        return view('component.add');

    }

    /**
     *
     * @return type
     */
    public function store(Request $request)
    {
        $component_tab = $request->input('component');
        $component_tab['price'] = $component_tab['price'] * 100;

        Component::create($component_tab);

        return redirect()->route('component.index')->with('success','Le composant a bien été ajouté!');
    }

    public function delete(Request $request)
    {
        $component = Component::find($request->input('id_component'));

        $component->delete();

        return redirect()->back()->with('success','Le composant a bien été supprimé!');
    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Component $component)
    {
        return view('component.show', [
            'component' => $component
        ]);
    }

    /**
     * @param ComponentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $component_tab = $request->input('component');
        $component_tab['price'] = $component_tab['price'] * 100;

        $component = Component::find($component_tab['id_component']);

        $component->update($component_tab);

        return redirect()->back()->with('success','Le composant a bien été modifié!');
    }

}
