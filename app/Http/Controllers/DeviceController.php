<?php

namespace App\Http\Controllers;

use App\Models\Component;
use App\Models\Device;
use App\Models\DeviceComponent;
use App\Models\DeviceType;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $devices = Device::orderBy('id_device','desc')->get();
        return view('device.index',
            ['devices' => $devices]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $components = Component::orderBy('name_fr')->get();
        $deviceTypes = DeviceType::orderBy('name_fr')->get();
        return view('device.add',
            ['components' => $components,
                'deviceTypes' => $deviceTypes]
        );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $device_tab = $request->input('device');
        $device = Device::create($device_tab);

        foreach($request->input('id_component') as $id_component)
        {
            $device_component_tab = [
                "device_id" => $device->id_device,
                "component_id" => $id_component,
                "quantity" => 1
            ];
            DeviceComponent::create($device_component_tab);
        }

        return redirect()->route('device.index')->with('success','Le device a bien été ajouté!');
    }

    /**
     *      * Display the specified resource.
     *
     * @param Device $device
     * @return Application|Factory|View
     */
    public function show(Device $device)
    {
        return view('device.show', [
            'device' => $device
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $device_tab = $request->input('device');

        $device = Device::find($id);

        $device->update($device_tab);

        return redirect()->back()->with('success','Le device a bien été modifié!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $device = Device::find($id);

        $device_components = DeviceComponent::where("device_id", $id)->get();
        foreach($device_components as $device_component)
        {
            $device_component->delete();
        }

        $device->delete();

        return redirect()->back()->with('success','Le device a bien été supprimé!');
    }
}
