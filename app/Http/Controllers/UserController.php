<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Factory;
use \Illuminate\Support\Facades\Hash;
use \App\Models\User;

class UserController extends Controller
{

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $users = User::orderBy('last_name')->get();

        return view('user.index', [
            'users' => $users
        ]);
    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $factories = Factory::all();

        return view('user.add', [
            'factories' => $factories
        ]);
    }

    /**
     * 
     * @return type
     */
    public function store(Request $request)
    {
        
        $user_tab = $request->input('user');
        
        $user_tab['password'] = Hash::make($user_tab['password']);
        
        User::create($user_tab);

        return redirect()->route('user.index')->with('success','L\'user a bien été ajouté!');
    }
    
    public function delete(Request $request)
    {
        $user = User::find($request->input('user_id'));
        
        $user->delete();

        return redirect()->back()->with('success','L\'user a bien été supprimé!');
    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(User $user)
    {
        $factories = Factory::all();
        return view('user.show', [
            'user' => $user,
            'factories' => $factories
        ]);
    }

    /**
     * 
     * @return type
     */
    public function update(Request $request)
    {
        
             
        
        $user_tab = $request->input('user');
        
        $user = User::find($user_tab['user_id']);
        
        $user->update($user_tab);

        return redirect()->back()->with('success','L\'user a bien été modifié!');
    }

}
