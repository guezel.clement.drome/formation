<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Fonction pour retourner à la page d'index    
     * @return type
     */
    public function index()
    {
        return view('index');
    }
}
