<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){


        return view('order.index');

    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(){

        // Recupérer les composants

        return view('order.add');

    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statistique(){

        // Recupérer les composants

        return view('order.statistique');

    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function import(){

        // Recupérer les composants

        return view('order.import');

    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function export(){

        // Recupérer les composants

        return view('order.export');

    }

}
