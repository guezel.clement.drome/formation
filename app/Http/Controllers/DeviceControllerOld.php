<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeviceControllerOld extends Controller
{

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){


        return view('device.index');

    }

    /**
     * Fonction pour retourner la vue par defaut à la connexion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(){

        // Recupérer les composants

        return view('device.add');

    }

}
