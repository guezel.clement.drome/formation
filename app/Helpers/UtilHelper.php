<?php
namespace App\Helpers;

class UtilHelper
{
    /**
     * Retourne un prix joliment
     * @param $price
     * @return string
     */
    public static final function displayPrettyPrice($price)
    {
        return number_format($price/100, 2, ',', ' ');
    }
}
