<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderDevice
 * 
 * @property int $id_order_device
 * @property int|null $order_id
 * @property int|null $device_id
 * @property int|null $quantity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Device|null $device
 * @property Order|null $order
 *
 * @package App\Models
 */
class OrderDevice extends Model
{
	protected $table = 'order_device';
	protected $primaryKey = 'id_order_device';

	protected $casts = [
		'order_id' => 'int',
		'device_id' => 'int',
		'quantity' => 'int'
	];

	protected $fillable = [
		'order_id',
		'device_id',
		'quantity'
	];

	public function device()
	{
		return $this->belongsTo(Device::class);
	}

	public function order()
	{
		return $this->belongsTo(Order::class);
	}
}
