<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DeviceType
 *
 * @property int $id_device_type
 * @property string|null $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Device[] $devices
 *
 * @package App\Models
 */
class DeviceType extends Model
{
	protected $table = 'device_type';
	protected $primaryKey = 'id_device_type';

	protected $fillable = [
		'name_fr',
        'name_en',
        'name_de'
	];

	public function devices()
	{
		return $this->hasMany(Device::class);
	}
}
