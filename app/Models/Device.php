<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Device
 *
 * @property int $id_device
 * @property int|null $device_type_id
 * @property string|null $name
 * @property string|null $version
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property DeviceType|null $device_type
 * @property Collection|Component[] $components
 * @property Collection|Order[] $orders
 *
 * @package App\Models
 */
class Device extends Model
{
	protected $table = 'device';
	protected $primaryKey = 'id_device';

	protected $casts = [
		'device_type_id' => 'int'
	];

	protected $fillable = [
		'device_type_id',
        'name_fr',
        'name_en',
        'name_de',
		'version'
	];

	public function device_type()
	{
		return $this->belongsTo(DeviceType::class);
	}

	public function components()
	{
		return $this->belongsToMany(Component::class, 'device_component')
					->withPivot('id_device_component', 'quantity')
					->withTimestamps();
	}

	public function orders()
	{
		return $this->belongsToMany(Order::class, 'order_device')
					->withPivot('id_order_device', 'quantity')
					->withTimestamps();
	}
}
