<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Factory
 *
 * @property int $id_factory
 * @property string|null $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Order[] $orders
 * @property Collection|User[] $users
 *
 * @package App\Models
 */
class Factory extends Model
{
	protected $table = 'factory';
	protected $primaryKey = 'id_factory';

	protected $fillable = [
		'name'
	];

	public function orders()
	{
		return $this->hasMany(Order::class, 'factory_id');
	}

	public function users()
	{
		return $this->hasMany(User::class, 'factory_id');
	}
}
