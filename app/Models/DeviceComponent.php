<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DeviceComponent
 * 
 * @property int $id_device_component
 * @property int|null $device_id
 * @property int|null $component_id
 * @property int|null $quantity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Component|null $component
 * @property Device|null $device
 *
 * @package App\Models
 */
class DeviceComponent extends Model
{
	protected $table = 'device_component';
	protected $primaryKey = 'id_device_component';

	protected $casts = [
		'device_id' => 'int',
		'component_id' => 'int',
		'quantity' => 'int'
	];

	protected $fillable = [
		'device_id',
		'component_id',
		'quantity'
	];

	public function component()
	{
		return $this->belongsTo(Component::class);
	}

	public function device()
	{
		return $this->belongsTo(Device::class);
	}
}
