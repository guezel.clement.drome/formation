<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * 
 * @property int $id_order
 * @property int|null $factory_id
 * @property string|null $number
 * @property int|null $client_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Factory|null $factory
 * @property Collection|Device[] $devices
 *
 * @package App\Models
 */
class Order extends Model
{
	protected $table = 'order';
	protected $primaryKey = 'id_order';

	protected $casts = [
		'factory_id' => 'int',
		'client_id' => 'int'
	];

	protected $fillable = [
		'factory_id',
		'number',
		'client_id'
	];

	public function factory()
	{
		return $this->belongsTo(Factory::class);
	}

	public function devices()
	{
		return $this->belongsToMany(Device::class, 'order_device')
					->withPivot('id_order_device', 'quantity')
					->withTimestamps();
	}
}
