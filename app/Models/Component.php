<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Component
 *
 * @property int $id_component
 * @property string|null $name
 * @property int|null $price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Device[] $devices
 *
 * @package App\Models
 */
class Component extends Model
{
	protected $table = 'component';
	protected $primaryKey = 'id_component';

	protected $casts = [
		'price' => 'int'
	];

	protected $fillable = [
        'name_fr',
        'name_en',
        'name_de',
		'price'
	];

	public function devices()
	{
		return $this->belongsToMany(Device::class, 'device_component')
					->withPivot('id_device_component', 'quantity')
					->withTimestamps();
	}
}
