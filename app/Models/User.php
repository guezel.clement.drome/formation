<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use SoftDeletes;
    use TwoFactorAuthenticatable;
   protected $table = 'users';

	protected $casts = [
		'factory_id' => 'int',
		'current_team_id' => 'int',
		'is_admin' => 'bool'
	];

	protected $dates = [
		'email_verified_at',
		'deleted_at'
	];

	protected $hidden = [
		'password',
		'two_factor_secret',
		'remember_token'
	];

	protected $fillable = [
		'factory_id',
		'first_name',
		'last_name',
		'email',
		'email_verified_at',
		'password',
		'two_factor_secret',
		'two_factor_recovery_codes',
		'remember_token',
		'current_team_id',
		'profile_photo_path',
		'is_admin'
	];

        
        
        // ########################################################
        // ########################################################
        // HasMany
        // ########################################################
        // ########################################################
        
        
        
        // ########################################################
        // ########################################################
        // BelongsTO
        // ########################################################
        // ########################################################
        /**
         * PErmet de récuprer l'usine pour l'utilisateur courant
         * @return type
         */
	public function factory()
	{
            return $this->belongsTo(Factory::class, 'factory_id');
	}
        
        
        
        
        // ########################################################
        // ########################################################
        // Getters
        // ########################################################
        // ########################################################
        
}
