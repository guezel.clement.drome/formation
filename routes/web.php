<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::middleware(['auth'])->group(function ()
{
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

    /**
     * Route pour les user
     */
    Route::group(['prefix' => 'user'], function() {
        Route::get('/', ['as' => 'user.index', 'uses' => 'UserController@index']);
        Route::get('/add', ['as' => 'user.add', 'uses' => 'UserController@add']);
        Route::post('/store', ['as' => 'user.store', 'uses' => 'UserController@store']);
        Route::delete('/delete', ['as' => 'user.delete', 'uses' => 'UserController@delete']);
        Route::get('/show/{user}', ['as' => 'user.show', 'uses' => 'UserController@show']);
        Route::post('/update', ['as' => 'user.update', 'uses' => 'UserController@update']);
    });

    /**
     * Route pour les component
     */
    Route::group(['prefix' => 'component'], function() {
        Route::get('/', ['as' => 'component.index', 'uses' => 'ComponentController@index']);
        Route::get('/add', ['as' => 'component.add', 'uses' => 'ComponentController@add']);
        Route::post('/store', ['as' => 'component.store', 'uses' => 'ComponentController@store']);
        Route::delete('/delete', ['as' => 'component.delete', 'uses' => 'ComponentController@delete']);
        Route::get('/show/{component}', ['as' => 'component.show', 'uses' => 'ComponentController@show']);
        Route::post('/update', ['as' => 'component.update', 'uses' => 'ComponentController@update']);
    });

    /**
     * Route pour les order
     */
    Route::group(['prefix' => 'order'], function() {
        Route::get('/', ['as' => 'order.index', 'uses' => 'OrderController@index']);
        Route::get('/add', ['as' => 'order.add', 'uses' => 'OrderController@add']);
        Route::get('/statistique', ['as' => 'order.statistique', 'uses' => 'OrderController@statistique']);
        Route::get('/import', ['as' => 'order.import', 'uses' => 'OrderController@import']);
        Route::get('/export', ['as' => 'order.export', 'uses' => 'OrderController@export']);
    });

    /**
     * Route pour les devices
     */
    Route::group(['prefix' => 'device'], function() {
        Route::get('/', ['as' => 'device.index', 'uses' => 'DeviceController@index']);
        Route::get('/create', ['as' => 'device.create', 'uses' => 'DeviceController@create']);
        Route::post('/store', ['as' => 'device.store', 'uses' => 'DeviceController@store']);
        Route::delete('/destroy/{id}', ['as' => 'device.destroy', 'uses' => 'DeviceController@destroy']);
        Route::post('/update/{id}', ['as' => 'device.update', 'uses' => 'DeviceController@update']);
        Route::get('/show/{device}', ['as' => 'device.show', 'uses' => 'DeviceController@show']);
    });
});

